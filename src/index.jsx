import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import Index from './ui/router/Index';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <React.StrictMode>
    <Index />
  </React.StrictMode>,

  document.getElementById('root'),
);

reportWebVitals();
