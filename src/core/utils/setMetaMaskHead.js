import ModelViewer from '@metamask/logo';

export default function setMetaMaskHead(block) {
  const viewer = ModelViewer({
    pxNotRatio: true,
    width: 150,
    height: 150,
    cameraDistance: 465,
    followMouse: true,
    slowDrift: false,
  });

  document.getElementById(block).innerHTML = '';
  document.getElementById(block).appendChild(viewer.container);
}
