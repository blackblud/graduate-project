export default async function changeNetwork() {
  await window.ethereum.request({
    method: 'wallet_switchEthereumChain',
    params: [
      {
        chainId: '0x4',
      },
    ],
  });
}
