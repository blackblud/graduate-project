const tokensImages = new Map();

tokensImages.set('ETH', 'https://assets.coingecko.com/coins/images/279/large/ethereum.png?1595348880');
tokensImages.set('UNI', 'https://assets.coingecko.com/coins/images/12504/large/uniswap-uni.png?1600306604');
tokensImages.set('MKR', 'https://assets.coingecko.com/coins/images/1364/large/Mark_Maker.png?1585191826');
tokensImages.set('LINK', 'https://assets.coingecko.com/coins/images/877/large/chainlink-new-logo.png?1547034700');
tokensImages.set('DAI', 'https://assets.coingecko.com/coins/images/9956/large/4943.png?1636636734');
tokensImages.set('USDT', 'https://assets.coingecko.com/coins/images/6319/large/USD_Coin_icon.png?1547042389');
tokensImages.set('USDC', 'https://assets.coingecko.com/coins/images/325/large/Tether-logo.png?1598003707');
tokensImages.set('BAT', 'https://assets.coingecko.com/coins/images/677/large/basic-attention-token.png?1547034427');
tokensImages.set('ETH -> NULP', 'https://assets.coingecko.com/coins/images/279/large/ethereum.png?1595348880');

export default tokensImages;
