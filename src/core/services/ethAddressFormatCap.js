export default function ethAddressFormatCap(text) {
  if (text === undefined) return;
  if (text === null) return;

  text = text.substring(0, 2) + text.substring(2, text.length);
  return text.substr(0, 1) + '×' + text.substr(2);
}
