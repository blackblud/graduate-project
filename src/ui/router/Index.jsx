/* eslint-disable no-unused-vars */
import React, { useState, createContext } from 'react';
import Cryptocurrencies from '../pages/Cryptocurrencies';
import CryptocurrenciesCategory from '../pages/CryptocurrenciesCategory';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import Header from '../components/Header';
import WhaleAlert from '../pages/WhaleAlert';
import Converter from '../pages/tools/Converter';
import Indicators from '../pages/tools/Indicators';
import Tools from '../pages/tools/Tools';
import Footer from '../components/Footer';
import Coin from '../pages/Coin';
import Wallet from '../pages/Wallet';
import Balance from '../pages/profile/Balance';
import NFT from '../pages/profile/NFT';
import Transaction from '../pages/profile/Transaction';
import Page404 from '../pages/Page404';

export const ConnectContext = createContext();

export default function Index() {
  const [isConnected, setIsConnected] = useState(false);
  const [networkChanged, setNetworkChanged] = useState(false);

  function toggleConnect() {
    setNetworkChanged((networkChanged) => !networkChanged);
  }

  return (
    <ConnectContext.Provider value={{ isConnected, setIsConnected, toggleConnect }}>
      <BrowserRouter>
        <Header />
        <Routes>
          <Route path="/" element={<Cryptocurrencies />} />
          <Route path="/cryptocurrencies" element={<Navigate to="/" />} />
          <Route path="/cryptocurrencies/:category" element={<CryptocurrenciesCategory />} />

          <Route path="/coin" element={<Navigate to="/" />} />
          <Route path="/coin/:coin_id" element={<Coin />} />

          <Route path="/whale-alert" element={<WhaleAlert />} />

          <Route path="/wallet" element={<Navigate to="/wallet/balance" />} />

          <Route path="/wallet" element={<Wallet />}>
            <Route path="balance" element={<Balance />} />
            <Route path="transaction" element={<Transaction />} />
            <Route path="nft" element={<NFT />} />
          </Route>

          <Route path="/tools/" element={<Tools />} />
          <Route path="/tools/converter" element={<Converter />} />
          <Route path="/tools/indicators" element={<Indicators />} />

          <Route path="*" element={<Page404 />} />
        </Routes>
        <Footer />
      </BrowserRouter>
    </ConnectContext.Provider>
  );
}
