import React from 'react';
import '../style/footer.scss';
import Logo from '../../public/images/Logo.png';

export default function Footer() {
  return (
    <footer>
      <div className="wrapper-footer">
        <div className="footer-logo">
          <img src={Logo} alt="Logo" />
          <p>CoinFolio</p>
        </div>
        <div className="footer-info">
          <p className="footer-right">&copy; 2022 Coinfolio. All Rights Reserved.</p>
          <p className="footer-provided">
            Market data provided by{' '}
            <a className="coingecko-link" href="https://www.coingecko.com/">
              CoinGecko.com
            </a>
          </p>
        </div>
      </div>
    </footer>
  );
}
