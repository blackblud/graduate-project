import React, { useState, useEffect, useContext } from 'react';
import '../style/header.scss';
import Logo from '../../public/images/Logo.png';
import MetaLogo from '../../public/images/MetamaskLogoWhite.png';
import { Link } from 'react-router-dom';
import { ethers } from 'ethers';
import ethAdressFormat from '../../core/services/ethAddressFormat';
import { BiExit } from 'react-icons/bi';
import { FiExternalLink } from 'react-icons/fi';
import capitalizeFirstLetter from '../../core/services/capFirstLetter';
import { ConnectContext } from '../router/Index';
import { RiLockPasswordLine } from 'react-icons/ri';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';
import { styled } from '@mui/material/styles';
import ethAddressFormatCap from '../../core/services/ethAddressFormatCap';
import { SiEthereum } from 'react-icons/si';

export default function Header() {
  const [accountAdress, setAccountAdress] = useState(null);
  const [isPassEntered, setIsPassEntered] = useState(false);

  const connectContext = useContext(ConnectContext);

  useEffect(() => {
    if (localStorage.getItem('MetaMask Address') === null) {
      connectContext.setIsConnected(false);
    } else {
      connectMetaMask();
    }
  }, []);

  async function connectMetaMask() {
    const [account] = await window.ethereum.request({ method: 'eth_requestAccounts' }).catch((error) => {
      if (error.code === -32002) setIsPassEntered(true);
    });

    setIsPassEntered(false);

    const provider = new ethers.providers.Web3Provider(window.ethereum);
    const balance = await provider.getBalance(account);

    setAccountAdress(account);
    localStorage.setItem('MetaMask Address', account);
    localStorage.setItem('MetaMask Balance', ethers.utils.formatEther(balance));
    localStorage.setItem('MetaMask ChainID', provider._network.chainId);
    localStorage.setItem('MetaMask ChainName', capitalizeFirstLetter(provider._network.name));

    connectContext.setIsConnected(true);
    connectContext.toggleConnect();
  }

  function disconnectMetaMask() {
    localStorage.removeItem('MetaMask Address');
    localStorage.removeItem('MetaMask Balance');
    localStorage.removeItem('MetaMask ChainID');
    localStorage.removeItem('MetaMask ChainName');
    localStorage.removeItem('Imported ERC20');
    localStorage.removeItem('Transaction History');
    connectContext.setIsConnected(false);
  }

  useEffect(() => {
    window?.ethereum?.on('accountsChanged', connectMetaMask);
    window?.ethereum?.on('chainChanged', connectMetaMask);
  }, []);

  const BootstrapTooltip = styled(({ className, ...props }) => <Tooltip {...props} arrow classes={{ popper: className }} />)(() => ({
    [`& .${tooltipClasses.arrow}`]: {
      color: '#363636',
    },
    [`& .${tooltipClasses.tooltip}`]: {
      backgroundColor: '#363636',
      fontSize: 14,
      maxWidth: 500,
      padding: 10,
    },
  }));

  return (
    <header>
      <div className="wrapper-header">
        <Link to="/" className="header-logo">
          <img src={Logo} alt="Logo" />
          <p>CoinFolio</p>
        </Link>
        <nav className="stroke">
          <ul className="ul-header">
            <li>
              <Link to="/" className="header-button">
                Cryptocurrencies
              </Link>
            </li>
            <li>
              <Link to="/whale-alert" className="header-button">
                WhaleAlert
              </Link>
            </li>
            <li>
              <Link to="/tools" className="header-button">
                Tools
              </Link>
            </li>
            <li>
              <Link to="/wallet/balance" className="header-button">
                My Wallet
              </Link>
            </li>
          </ul>
        </nav>

        {!window.ethereum ? (
          <a className="meta-connected" href={'https://metamask.io/'} target="_blank" rel="noreferrer">
            <img src={MetaLogo} alt="MetaMask" />
            Install MetaMask
            <FiExternalLink className="icon-header" />
          </a>
        ) : isPassEntered ? (
          <div
            className="meta-connected"
            onClick={() => {
              connectMetaMask();
            }}
          >
            <img src={MetaLogo} alt="MetaMask" />
            Enter Password
            <RiLockPasswordLine className="icon-header" />
          </div>
        ) : connectContext.isConnected ? (
          <div className="meta-connected" onClick={disconnectMetaMask}>
            <img src={MetaLogo} alt="MetaMask" />
            <BootstrapTooltip
              placement="bottom"
              arrow
              title={
                <div className="tooltip-header-button">
                  <SiEthereum />
                  {ethAddressFormatCap(accountAdress)}
                </div>
              }
            >
              <p>{ethAdressFormat(accountAdress)}</p>
            </BootstrapTooltip>
            <BiExit className="icon-header" />
          </div>
        ) : (
          <button onClick={connectMetaMask}>
            <img src={MetaLogo} alt="MetaMask" />
            Connect to MetaMask
          </button>
        )}
      </div>
    </header>
  );
}
