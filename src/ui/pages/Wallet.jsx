import React, { useState, useEffect, useContext, createContext } from 'react';
import '../style/profile.scss';
import { Link, Outlet, useLocation } from 'react-router-dom';
import { ConnectContext } from '../router/Index';
import ethAddressFormatCap from '../../core/services/ethAddressFormatCap';
import setMetaMaskHead from '../../core/utils/setMetaMaskHead';
import changeNetwork from '../../core/utils/changeNetwork';

import { BiNetworkChart } from 'react-icons/bi';
import { HiOutlineStatusOnline } from 'react-icons/hi';
import { SiEthereum } from 'react-icons/si';

import { MdAccountBalanceWallet } from 'react-icons/md';
import { FaMoneyBillWave } from 'react-icons/fa';
import { BsFileEarmarkImageFill } from 'react-icons/bs';
import Top100Tokens from '../../core/store/Top100Tokens';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';
import { styled } from '@mui/material/styles';

export const TokensContext = createContext();

export default function Wallet() {
  const [accountAddress, setAccountAddress] = useState('0x0000000000000000000000000000000000000000');
  const [accountBalance, setAccountBalance] = useState(null);
  const [chainID, setChainID] = useState(null);
  const [chainName, setChainName] = useState(null);

  const [ERC20List, setERC20List] = useState(Top100Tokens);

  const connectContext = useContext(ConnectContext);
  const path = useLocation();

  useEffect(() => {
    if (localStorage.getItem('MetaMask Address') !== null) {
      setAccountAddress(localStorage.getItem('MetaMask Address'));
      setAccountBalance(localStorage.getItem('MetaMask Balance'));
      setChainID(localStorage.getItem('MetaMask ChainID'));
      setChainName(localStorage.getItem('MetaMask ChainName'));
    } else {
      setAccountAddress('0x0000000000000000000000000000000000000000');
      setAccountBalance(null);
      setChainID(null);
      setChainName(null);
    }
  }, [connectContext]);

  const elements = Array.from(document.querySelectorAll('.outlet-buttons'));

  let activeIndex = 0;

  const updateIndex = (newIndex) => {
    elements[activeIndex]?.classList.remove('active');
    document.body.style.setProperty('--active-index', newIndex);
    elements[newIndex]?.classList.add('active');
    activeIndex = newIndex;
  };

  const registerEvent = (button, index) => {
    button.addEventListener('click', () => updateIndex(index));
  };

  elements.forEach(registerEvent);

  if (path.pathname === '/wallet/balance') {
    updateIndex(0);
  } else if (path.pathname === '/wallet/transaction') {
    updateIndex(1);
  } else if (path.pathname === '/wallet/nft') {
    updateIndex(2);
  }

  useEffect(() => {
    setMetaMaskHead('head-block');
  }, []);

  const BootstrapTooltip = styled(({ className, ...props }) => <Tooltip {...props} arrow classes={{ popper: className }} />)(() => ({
    [`& .${tooltipClasses.arrow}`]: {
      color: '#363636',
    },
    [`& .${tooltipClasses.tooltip}`]: {
      backgroundColor: '#363636',
      fontSize: 14,
    },
  }));

  return (
    <div className="wrapper">
      <div className="profile-page">
        <div className="profile-info">
          <div className="profile-info-block">
            {chainID ? (
              chainID === '4' ? (
                <div className="info-status-block">
                  <BiNetworkChart className="status-block-icon" />
                  Network: #{chainID} {chainName}
                </div>
              ) : chainID === '31337' || chainID === '1337' ? (
                <div className="info-status-block">
                  <BiNetworkChart className="status-block-icon" />
                  Test Network: #{chainID} Hardhat
                </div>
              ) : (
                <div className="info-status-block wrong-network" onClick={changeNetwork}>
                  <BiNetworkChart className="status-block-icon" />
                  Wrong Network: #{chainID} {chainName}
                </div>
              )
            ) : (
              <div className="info-status-block">
                <BiNetworkChart className="status-block-icon" />
                Network: Unknown
              </div>
            )}
            {connectContext.isConnected ? (
              <div className="info-status-block">
                <HiOutlineStatusOnline className="status-block-icon" />
                Status: Connected
                <div className="status background-green"></div>
              </div>
            ) : (
              <div className="info-status-block">
                <HiOutlineStatusOnline className="status-block-icon" />
                Status: Disconnected
                <div className="status background-red"></div>
              </div>
            )}
          </div>

          <div className="info-head">
            <hr className="head-hr" />
            <div id="head-block"></div>
          </div>

          <div className="info-address">
            <BootstrapTooltip enterDelay={500} enterNextDelay={500} placement="bottom" arrow title={accountBalance + ' ETH'}>
              <a href={'https://rinkeby.etherscan.io/address/' + accountAddress} target="_blank" rel="noreferrer">
                <div className="info-status-block trans-cursor">
                  <SiEthereum className="status-block-icon" />
                  {ethAddressFormatCap(accountAddress)}
                </div>
              </a>
            </BootstrapTooltip>
          </div>
        </div>

        <div className="profile-links">
          {path.pathname === '/wallet/balance' ? (
            <div className="container-outlet">
              <Link to="/wallet/balance" className="button active outlet-buttons" id="1">
                <MdAccountBalanceWallet className="outlet-icon-balance" />
                Balance
              </Link>
              <Link to="/wallet/transaction" className="button outlet-buttons" id="2">
                <FaMoneyBillWave className="outlet-icon-trans" />
                Transaction
              </Link>
              <Link to="/wallet/nft" className="button outlet-buttons" id="3">
                <BsFileEarmarkImageFill className="outlet-icon-nft" />
                NFT
              </Link>
              <div className="indicator-outlet"></div>
            </div>
          ) : path.pathname === '/wallet/transaction' ? (
            <div className="container-outlet">
              <Link to="/wallet/balance" className="button outlet-buttons" id="1">
                <MdAccountBalanceWallet className="outlet-icon-balance" />
                Balance
              </Link>
              <Link to="/wallet/transaction" className="button active outlet-buttons" id="2">
                <FaMoneyBillWave className="outlet-icon-trans" />
                Transaction
              </Link>
              <Link to="/wallet/nft" className="button outlet-buttons" id="3">
                <BsFileEarmarkImageFill className="outlet-icon-nft" />
                NFT
              </Link>
              <div className="indicator-outlet"></div>
            </div>
          ) : (
            <div className="container-outlet">
              <Link to="/wallet/balance" className="button outlet-buttons" id="1">
                <MdAccountBalanceWallet className="outlet-icon-balance" />
                Balance
              </Link>
              <Link to="/wallet/transaction" className="button outlet-buttons" id="2">
                <FaMoneyBillWave className="outlet-icon-trans" />
                Transaction
              </Link>
              <Link to="/wallet/nft" className="button active outlet-buttons" id="3">
                <BsFileEarmarkImageFill className="outlet-icon-nft" />
                NFT
              </Link>
              <div className="indicator-outlet"></div>
            </div>
          )}
        </div>

        {window.ethereum ? (
          <TokensContext.Provider value={{ ERC20List, setERC20List }}>
            <div className="profile-content">
              {connectContext.isConnected ? <Outlet /> : <div className="profile-install-message">It looks like you don&rsquo;t have MetaMask connected.</div>}
            </div>
          </TokensContext.Provider>
        ) : !connectContext.isConnected ? (
          <div className="profile-install-message">It looks like you don&rsquo;t have MetaMask installed.</div>
        ) : (
          <div className="profile-install-message">It looks like you don&rsquo;t have MetaMask connected.</div>
        )}
      </div>
    </div>
  );
}
