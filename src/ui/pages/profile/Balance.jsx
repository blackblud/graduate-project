import { ethers } from 'ethers';
import React, { useState, useEffect, useContext } from 'react';
import ERC20ABI from '../../../core/config/ERC20ABI.json';
import { ConnectContext } from '../../router/Index';
import ethAddressFormatCap from '../../../core/services/ethAddressFormatCap';
import numberCommasCrypto from '../../../core/services/numberCommasCrypto';
import tokensImages from '../../../core/store/tokensImages';
import { TokensContext } from '../Wallet';
import UnknownIcon from '../../../public/images/UnknownCoin.png';
import SpinerLoader from '../../../public/images/SpinerLoader.gif';

export default function Balance() {
  const [accountAddress, setAccountAddress] = useState(null);
  const [accountBalance, setAccountBalance] = useState(null);

  const [ERC20Data, setERC20Data] = useState([]);

  const connectContext = useContext(ConnectContext);
  const tokensContext = useContext(TokensContext);

  useEffect(() => {
    if (localStorage.getItem('MetaMask Address') !== null) {
      setAccountAddress(localStorage.getItem('MetaMask Address'));
      setAccountBalance(localStorage.getItem('MetaMask Balance'));
      getBalanceERC20();
    }
  }, [connectContext]);

  useEffect(() => {
    getBalanceERC20();
  }, [tokensContext.ERC20List, ConnectContext]);

  async function getBalanceERC20() {
    document.querySelector('#loader-anim-eth')?.classList.add('display-none');
    document.querySelector('#loader-anim-table')?.classList.remove('display-none');

    let listERC20 = [];

    for (let token of tokensContext.ERC20List) {
      const contract = new ethers.Contract(token, ERC20ABI, new ethers.providers.Web3Provider(window.ethereum));
      const balance = await contract.balanceOf(localStorage.getItem('MetaMask Address'));
      const symbol = await contract.symbol();
      const name = await contract.name();

      if (ethers.utils.formatEther(balance) > 0) {
        listERC20.push({
          name: name,
          symbol: symbol,
          value: ethers.utils.formatEther(balance),
          address: token,
        });
      }
    }

    //Якщо LocalStorage  не пустий то запустити цикл і дописати їх в listERC20
    if (localStorage.getItem('Imported ERC20') !== null) {
      var localStorageERC20 = JSON.parse(localStorage.getItem('Imported ERC20'));

      for (let token of localStorageERC20) {
        const contract = new ethers.Contract(token, ERC20ABI, new ethers.providers.Web3Provider(window.ethereum));
        const balance = await contract.balanceOf(localStorage.getItem('MetaMask Address'));
        const symbol = await contract.symbol();
        const name = await contract.name();

        if (ethers.utils.formatEther(balance) > 0) {
          listERC20.push({
            name: name,
            symbol: symbol,
            value: ethers.utils.formatEther(balance),
            address: token,
          });
        }
      }
    }

    setERC20Data(listERC20);
    document.querySelector('#import-load')?.classList.add('display-none');
    document.querySelector('#import-load-anim')?.classList.add('display-none');

    document.querySelector('#loader-anim-eth')?.classList.remove('display-none');
    document.querySelector('#loader-anim-table')?.classList.add('display-none');
  }

  //////////////////////////

  function importToken() {
    if (document.querySelector('#import-token').value === null) return;
    const address = document.querySelector('#import-token').value;

    if (!ethers.utils.getAddress(address)) {
      alert('Wrong Address. Please check again.');
      return;
    }

    if (tokensContext.ERC20List.includes(address)) {
      alert('Address already is in the Top 100 ERC20 Tokens.');
      return;
    }

    var localStorageERC20;

    if (localStorage.getItem('Imported ERC20') !== null) {
      const lsdata = JSON.parse(localStorage.getItem('Imported ERC20'));
      if (lsdata.includes(address)) {
        alert('Address already imported. Please notice that.');
        return;
      }

      localStorageERC20 = JSON.parse(localStorage.getItem('Imported ERC20'));
      localStorageERC20.push(address);
      localStorage.setItem('Imported ERC20', JSON.stringify(localStorageERC20));
    } else {
      const tempAddress = [];
      tempAddress[0] = address;
      localStorage.setItem('Imported ERC20', JSON.stringify(tempAddress));
    }

    getBalanceERC20();
    // tokensContext.setERC20List((ERC20List) => [...ERC20List, address]);
    document.querySelector('#import-load').classList.remove('display-none');
    document.querySelector('#import-load-anim').classList.remove('display-none');
    document.querySelector('#import-token').value = '';
  }

  return (
    <div>
      <table>
        <colgroup>
          <col />
          <col style={{ width: 210 + 'px' }} />
          <col />
          <col />
          <col style={{ width: 50 + 'px' }} />
        </colgroup>
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Balance</th>
            <th>Symbol</th>
            <th>Address</th>
          </tr>
        </thead>
        {ERC20Data.length === 0 ? (
          <tbody>
            <tr id="loader-anim-eth" className="display-none">
              <td className="currency-rank">1</td>
              <td>
                <div className="transaction-name">
                  <img className="transaction-image" src="https://assets.coingecko.com/coins/images/279/large/ethereum.png?1595348880" alt="" />
                  <p>Ethereum</p>
                </div>
              </td>
              <td>{accountBalance}</td>
              <td>ETH</td>
              <td>
                <a href={'https://rinkeby.etherscan.io/address/' + accountAddress} target="_blank" rel="noreferrer">
                  Ethereum Does&rsquo;nt Have a Smart-Contract
                </a>
              </td>
            </tr>
            <tr className="loader-anim-row" id="loader-anim-table">
              <td colSpan="11">
                <div className="loader-anim-profile">
                  <span></span>
                  <span></span>
                  <span></span>
                  <span></span>
                  <span></span>
                  <span></span>
                  <span></span>
                  <span></span>
                </div>
              </td>
            </tr>
          </tbody>
        ) : (
          <tbody>
            <tr>
              <td className="currency-rank">1</td>
              <td>
                <div className="transaction-name">
                  <img className="transaction-image" src="https://assets.coingecko.com/coins/images/279/large/ethereum.png?1595348880" alt="" />
                  <p>Ethereum</p>
                </div>
              </td>
              <td>{accountBalance}</td>
              <td>ETH</td>
              <td>
                <a href={'https://rinkeby.etherscan.io/address/' + accountAddress} target="_blank" rel="noreferrer">
                  Ethereum Does&rsquo;nt Have a Smart-Contract
                </a>
              </td>
            </tr>
            {ERC20Data.map((balance, id) => (
              <tr key={id}>
                <td className="currency-rank">{id + 2}</td>
                <td>
                  <div className="transaction-name">
                    {tokensImages.get(balance.symbol) ? (
                      <img className="transaction-image" src={tokensImages.get(balance.symbol)} alt="" />
                    ) : (
                      <img className="transaction-image" src={UnknownIcon} alt="" />
                    )}

                    <p>{balance.name}</p>
                  </div>
                </td>
                <td>{numberCommasCrypto(balance.value)}</td>
                <td>{balance.symbol}</td>
                <td className="trans-cursor">
                  <a href={'https://rinkeby.etherscan.io/token/' + balance.address} target="_blank" rel="noreferrer">
                    {ethAddressFormatCap(balance.address)}
                  </a>
                </td>
              </tr>
            ))}
          </tbody>
        )}
      </table>

      <table id="import-load" className="display-none">
        <colgroup>
          <col style={{ width: 38 + 'px' }} />
          <col style={{ width: 210 + 'px' }} />
          <col style={{ width: 305 + 'px' }} />
          <col style={{ width: 90 + 'px' }} />
          <col style={{ width: 411 + 'px' }} />
        </colgroup>
        <tbody>
          <tr className="import-table">
            <td className="currency-rank blured-text-token">0</td>
            <td className="blured-text-token">
              <div className="transaction-name">
                <img className="transaction-image blured-photo-token" src={UnknownIcon} alt="" />
                <p>Token Name</p>
              </div>
            </td>
            <td className="blured-text-token">{accountBalance}</td>
            <td className="blured-text-token">AAA</td>
            <td className="blured-text-token">0×1F9840A85D5AF5BF1D1762F925BDADDC4201F984</td>
          </tr>
        </tbody>
      </table>

      <div className="import-token-block">
        <div className="import-token-effect-block">
          <input
            className="import-token-effect"
            type="text"
            id="import-token"
            defaultValue="0xbF7A7169562078c96f0eC1A8aFD6aE50f12e5A99"
            placeholder="Enter Token's Contract-Address"
          />
          <span className="focus-border"></span>
        </div>
        <button onClick={importToken} className="import-token-button-block">
          Import Token
          <img src={SpinerLoader} alt="" id="import-load-anim" className="display-none" />
        </button>
      </div>
    </div>
  );
}
