import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { ethers } from 'ethers';
import { FiExternalLink } from 'react-icons/fi';
import PoliCoinABI from '../../../artifacts/contracts/PolyCoin.sol/PolyCoin.json';
import { BsInfo } from 'react-icons/bs';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';
import { styled } from '@mui/material/styles';
import timestampDateYear from '../../../core/services/timestampDateYear';
import Fade from '@mui/material/Fade';
import rarityLevel from '../../../core/services/rarityLevel';

import { IoColorPalette, IoLanguage, IoColorPaletteOutline } from 'react-icons/io5';
import { AiFillStar, AiOutlineStar, AiFillPicture, AiOutlinePicture } from 'react-icons/ai';
import { IoMdColorFill } from 'react-icons/io';
import { BsCalendarDate, BsCalendarDateFill } from 'react-icons/bs';
import { GiConvergenceTarget } from 'react-icons/gi';
import { MdDescription, MdOutlineDescription } from 'react-icons/md';
import { FiPercent } from 'react-icons/fi';

export default function PolyCoin({ tokenID }) {
  const provider = new ethers.providers.Web3Provider(window.ethereum);
  const signer = provider.getSigner();
  const contract = new ethers.Contract('0x9b88b0d2da010a5767a840085a5a7adb561daf65', PoliCoinABI.abi, signer); //0x5FbDB2315678afecb367f032d93F642f64180aa3

  const contentID = 'Qmd8s77m3Yb9gzqUYPMRLmt9fD7DvBiYFbD9dN1mfng4GB';
  const metadataURI = `${contentID}/${tokenID}.json`;

  var NFTURI;
  var imageURI;

  if (tokenID === 4 || tokenID === 9 || tokenID === 26 || tokenID === 40 || tokenID === 49 || tokenID === 65) {
    NFTURI = `https://gateway.pinata.cloud/ipfs/${contentID}/${tokenID}.gif`;
    imageURI = `/NFT/${tokenID}.gif`;
  } else {
    NFTURI = `https://gateway.pinata.cloud/ipfs/${contentID}/${tokenID}.png`;
    imageURI = `/NFT/${tokenID}.png`;
  }

  const jsonURI = `/NFT Metadata/${tokenID}.json`;

  const NFTTooltip = styled(({ className, ...props }) => (
    <Tooltip {...props} arrow TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} classes={{ popper: className }} />
  ))(() => ({
    [`& .${tooltipClasses.arrow}`]: {
      color: '#eff2f5;',
    },
    [`& .${tooltipClasses.tooltip}`]: {
      maxWidth: 500,
      paddingLeft: 10,
      paddingRight: 10,
      paddingTop: 10,
      borderRadius: 10,
      color: '#363636',
      backgroundColor: '#fff;',
      boxShadow: 'rgba(0, 0, 0, 0.24) 0px 3px 8px',
    },
  }));

  const UkrNFTTooltip = styled(({ className, ...props }) => (
    <Tooltip {...props} arrow TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} classes={{ popper: className }} />
  ))(() => ({
    [`& .${tooltipClasses.arrow}`]: {
      color: '#eff2f5;',
    },
    [`& .${tooltipClasses.tooltip}`]: {
      maxWidth: 500,
      paddingLeft: 10,
      paddingRight: 10,
      paddingTop: 10,
      borderRadius: 10,
      color: '#fff',
      boxShadow: 'rgba(0, 0, 0, 0.24) 0px 3px 8px',
      background: 'linear-gradient(180deg, #0057B7, #FFDD00)',
    },
  }));

  const UpaNFTTooltip = styled(({ className, ...props }) => (
    <Tooltip {...props} arrow TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} classes={{ popper: className }} />
  ))(() => ({
    [`& .${tooltipClasses.arrow}`]: {
      color: '#eff2f5;',
    },
    [`& .${tooltipClasses.tooltip}`]: {
      maxWidth: 500,
      paddingLeft: 10,
      paddingRight: 10,
      paddingTop: 10,
      borderRadius: 10,
      color: '#fff',
      boxShadow: 'rgba(0, 0, 0, 0.24) 0px 3px 8px',
      background: 'linear-gradient(180deg, #CC0000, #000000)',
    },
  }));

  const [metadata, setMetadata] = useState([
    {
      id: 0,
      name: 'PolyCoin #0',
      description: 'Polytech Design #0/60',
      date: 1648771200,
      attributes: [
        {
          trait_type: 'Text Color',
          value: 'Unknown',
        },
        {
          trait_type: 'Background Color',
          value: 'Unknown',
        },
        {
          trait_type: 'Language',
          value: 'Unknown',
        },
        {
          trait_type: 'Special Edition',
          value: 'Unknown',
        },
      ],
    },
  ]);

  useEffect(() => {
    axios
      .get(jsonURI)
      .then((res) => {
        setMetadata(res.data);
      })
      .catch((error) => {
        console.log(error.response);
      });
  }, []);

  const [isOwned, setIsOwned] = useState(false);

  useEffect(() => {
    isOwnedNFTStatus();
  }, [isOwned]);

  async function isOwnedNFTStatus() {
    const result = await contract.isContentOwned(localStorage.getItem('MetaMask Address'), metadataURI);
    setIsOwned(result);
  }

  return (
    <>
      {isOwned ? (
        <div className="nft-block">
          {metadata.attributes !== undefined ? (
            metadata.attributes[3]['value'] ? (
              metadata.attributes[1]['value'] === 'Ukraine Flag' ? (
                <UkrNFTTooltip
                  followCursor={true}
                  placement="bottom"
                  arrow
                  title={
                    <div className="tooltip-nft-info">
                      <div className="tooltip-nft-info-block">
                        <p>Basic Info:</p>
                        <div className="tooltip-div-flex-white">
                          <GiConvergenceTarget /> Gen -&nbsp;<span>{metadata?.gen}</span>
                        </div>
                        <div className="tooltip-div-flex-white">
                          <AiFillPicture />
                          Name -&nbsp;<span>{metadata?.name}</span>
                        </div>
                        <div className="tooltip-div-flex-white">
                          <BsCalendarDateFill className="nft-calendar-icon" />
                          Date Created -&nbsp;<span>{timestampDateYear(metadata?.date)}</span>
                        </div>
                        <div className="tooltip-div-flex-white">
                          <MdDescription />
                          Description -&nbsp;<span>{metadata?.description}</span>
                        </div>
                      </div>
                      <hr />
                      <div className="tooltip-nft-info-block">
                        <p>Attributes Info:</p>
                        <div className="tooltip-div-flex-white">
                          <IoColorPalette />
                          Text Color -&nbsp;<span>{metadata?.attributes[0]['value']}</span>
                        </div>
                        <div className="tooltip-div-flex-white">
                          <IoLanguage />
                          Text Language -&nbsp;<span>{metadata?.attributes[2]['value']}</span>
                        </div>
                        <div className="tooltip-div-flex-white">
                          <IoMdColorFill />
                          Background Color -&nbsp;<span>{metadata?.attributes[1]['value']}</span>
                        </div>
                        {metadata?.attributes[3]['value'] ? (
                          <div className="tooltip-div-flex-white">
                            <AiFillStar />
                            Special Edition -&nbsp;<span>Yes</span>
                          </div>
                        ) : (
                          <div className="tooltip-div-flex-white">
                            <AiFillStar />
                            Special Edition -&nbsp;<span>No</span>
                          </div>
                        )}
                      </div>
                      <hr />
                      <div className="tooltip-nft-info-block">
                        <div className="tooltip-percent-flex">
                          Rarity Level:&nbsp;
                          <span>{rarityLevel(metadata)}</span>
                          <FiPercent />
                        </div>
                        <input id="range-nft" type="range" min="0" max="100" value={rarityLevel(metadata)} onChange={() => {}} />
                      </div>
                    </div>
                  }
                >
                  <div className="nft-block-info">
                    <BsInfo />
                  </div>
                </UkrNFTTooltip>
              ) : metadata.attributes[1]['value'] === 'UPA Flag' ? (
                <UpaNFTTooltip
                  followCursor={true}
                  placement="bottom"
                  arrow
                  title={
                    <div className="tooltip-nft-info">
                      <div className="tooltip-nft-info-block">
                        <p>Basic Info:</p>
                        <div className="tooltip-div-flex-white">
                          <GiConvergenceTarget /> Gen -&nbsp;<span>{metadata?.gen}</span>
                        </div>
                        <div className="tooltip-div-flex-white">
                          <AiFillPicture />
                          Name -&nbsp;<span>{metadata?.name}</span>
                        </div>
                        <div className="tooltip-div-flex-white">
                          <BsCalendarDateFill className="nft-calendar-icon" />
                          Date Created -&nbsp;<span>{timestampDateYear(metadata?.date)}</span>
                        </div>
                        <div className="tooltip-div-flex-white">
                          <MdDescription />
                          Description -&nbsp;<span>{metadata?.description}</span>
                        </div>
                      </div>
                      <hr />
                      <div className="tooltip-nft-info-block">
                        <p>Attributes Info:</p>
                        <div className="tooltip-div-flex-white">
                          <IoColorPalette />
                          Text Color -&nbsp;<span>{metadata?.attributes[0]['value']}</span>
                        </div>
                        <div className="tooltip-div-flex-white">
                          <IoLanguage />
                          Text Language -&nbsp;<span>{metadata?.attributes[2]['value']}</span>
                        </div>
                        <div className="tooltip-div-flex-white">
                          <IoMdColorFill />
                          Background Color -&nbsp;<span>{metadata?.attributes[1]['value']}</span>
                        </div>
                        {metadata?.attributes[3]['value'] ? (
                          <div className="tooltip-div-flex-white">
                            <AiFillStar />
                            Special Edition -&nbsp;<span>Yes</span>
                          </div>
                        ) : (
                          <div className="tooltip-div-flex-white">
                            <AiFillStar />
                            Special Edition -&nbsp;<span>No</span>
                          </div>
                        )}
                      </div>
                      <hr />
                      <div className="tooltip-nft-info-block">
                        <div className="tooltip-percent-flex">
                          Rarity Level:&nbsp;
                          <span>{rarityLevel(metadata)}</span>
                          <FiPercent />
                        </div>
                        <input id="range-nft" type="range" min="0" max="100" value={rarityLevel(metadata)} onChange={() => {}} />
                      </div>
                    </div>
                  }
                >
                  <div className="nft-block-info">
                    <BsInfo />
                  </div>
                </UpaNFTTooltip>
              ) : (
                <NFTTooltip
                  followCursor={true}
                  placement="bottom"
                  arrow
                  title={
                    <div className="tooltip-nft-info">
                      <div className="tooltip-nft-info-block">
                        <p>Basic Info:</p>
                        <div className="tooltip-div-flex">
                          <GiConvergenceTarget /> Gen -&nbsp;<span>{metadata?.gen}</span>
                        </div>
                        <div className="tooltip-div-flex">
                          <AiOutlinePicture />
                          Name -&nbsp;<span>{metadata?.name}</span>
                        </div>
                        <div className="tooltip-div-flex">
                          <BsCalendarDate className="nft-calendar-icon" />
                          Date Created -&nbsp;<span>{timestampDateYear(metadata?.date)}</span>
                        </div>
                        <div className="tooltip-div-flex">
                          <MdOutlineDescription />
                          Description -&nbsp;<span>{metadata?.description}</span>
                        </div>
                      </div>
                      <hr />
                      <div className="tooltip-nft-info-block">
                        <p>Attributes Info:</p>
                        <div className="tooltip-div-flex">
                          <IoColorPaletteOutline />
                          Text Color -&nbsp;<span>{metadata?.attributes[0]['value']}</span>
                        </div>
                        <div className="tooltip-div-flex">
                          <IoLanguage />
                          Text Language -&nbsp;<span>{metadata?.attributes[2]['value']}</span>
                        </div>
                        <div className="tooltip-div-flex">
                          <IoMdColorFill />
                          Background Color -&nbsp;<span>{metadata?.attributes[1]['value']}</span>
                        </div>
                        {metadata?.attributes[3]['value'] ? (
                          <div className="tooltip-div-flex">
                            <AiOutlineStar />
                            Special Edition -&nbsp;<span>Yes</span>
                          </div>
                        ) : (
                          <div className="tooltip-div-flex">
                            <AiOutlineStar />
                            Special Edition -&nbsp;<span>No</span>
                          </div>
                        )}
                      </div>
                      <hr />
                      <div className="tooltip-nft-info-block">
                        <div className="tooltip-percent-flex">
                          Rarity Level:&nbsp;
                          <span>{rarityLevel(metadata)}</span>
                          <FiPercent />
                        </div>
                        <input id="range-nft" type="range" min="0" max="100" value={rarityLevel(metadata)} onChange={() => {}} />
                      </div>
                    </div>
                  }
                >
                  <div className="nft-block-info">
                    <BsInfo />
                  </div>
                </NFTTooltip>
              )
            ) : (
              <NFTTooltip
                followCursor={true}
                placement="bottom"
                arrow
                title={
                  <div className="tooltip-nft-info">
                    <div className="tooltip-nft-info-block">
                      <p>Basic Info:</p>
                      <div className="tooltip-div-flex">
                        <GiConvergenceTarget /> Gen -&nbsp;<span>{metadata?.gen}</span>
                      </div>
                      <div className="tooltip-div-flex">
                        <AiOutlinePicture />
                        Name -&nbsp;<span>{metadata?.name}</span>
                      </div>
                      <div className="tooltip-div-flex">
                        <BsCalendarDate className="nft-calendar-icon" />
                        Date Created -&nbsp;<span>{timestampDateYear(metadata?.date)}</span>
                      </div>
                      <div className="tooltip-div-flex">
                        <MdOutlineDescription />
                        Description -&nbsp;<span>{metadata?.description}</span>
                      </div>
                    </div>
                    <hr />
                    <div className="tooltip-nft-info-block">
                      <p>Attributes Info:</p>
                      <div className="tooltip-div-flex">
                        <IoColorPaletteOutline />
                        Text Color -&nbsp;<span>{metadata?.attributes[0]['value']}</span>
                      </div>
                      <div className="tooltip-div-flex">
                        <IoLanguage />
                        Text Language -&nbsp;<span>{metadata?.attributes[2]['value']}</span>
                      </div>
                      <div className="tooltip-div-flex">
                        <IoMdColorFill />
                        Background Color -&nbsp;<span>{metadata?.attributes[1]['value']}</span>
                      </div>
                      {metadata?.attributes[3]['value'] ? (
                        <div className="tooltip-div-flex">
                          <AiOutlineStar />
                          Special Edition -&nbsp;<span>Yes</span>
                        </div>
                      ) : (
                        <div className="tooltip-div-flex">
                          <AiOutlineStar />
                          Special Edition -&nbsp;<span>No</span>
                        </div>
                      )}
                    </div>
                    <hr />
                    <div className="tooltip-nft-info-block">
                      <div className="tooltip-percent-flex">
                        Rarity Level:&nbsp;
                        <span>{rarityLevel(metadata)}</span>
                        <FiPercent />
                      </div>
                      <input id="range-nft" type="range" min="0" max="100" value={rarityLevel(metadata)} onChange={() => {}} />
                    </div>
                  </div>
                }
              >
                <div className="nft-block-info">
                  <BsInfo />
                </div>
              </NFTTooltip>
            )
          ) : (
            ''
          )}
          <div className="nft-image">
            <img src={imageURI} alt="" />
          </div>
          <div className="nft-info">
            <p>#{metadata.id}</p>
            <a href={NFTURI} target="_blank" rel="noreferrer">
              View <FiExternalLink className="nft-url-icon" />
            </a>
          </div>
        </div>
      ) : (
        ''
      )}
    </>
  );
}
