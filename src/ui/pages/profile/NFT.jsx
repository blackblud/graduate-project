import React, { useState, useEffect, useContext } from 'react';
import { ethers } from 'ethers';
import { SiEthereum } from 'react-icons/si';
import { ConnectContext } from '../../router/Index';
import BlurredNewNFT from '../../../public/images/BlurredNewNFT.gif';
import PolyCoin from './PolyCoin';
import { IoIosArrowUp } from 'react-icons/io';
import { FiExternalLink } from 'react-icons/fi';

import PoliCoinABI from '../../../artifacts/contracts/PolyCoin.sol/PolyCoin.json';

export default function NFT() {
  const [totalNFT, setTotalNFT] = useState(0);
  const [balanceCount, setBalanceCount] = useState(0);

  const connectContext = useContext(ConnectContext);

  useEffect(() => {
    setTotalNFT(0);
    setBalanceCount(0);
    getCountNFT();
    getBalanceCount();
  }, [connectContext]);

  const provider = new ethers.providers.Web3Provider(window.ethereum);
  const signer = provider.getSigner();
  const contract = new ethers.Contract('0x9b88b0d2da010a5767a840085a5a7adb561daf65', PoliCoinABI.abi, signer); //0x5FbDB2315678afecb367f032d93F642f64180aa3

  async function getCountNFT() {
    const count = await contract.count();
    setTotalNFT(parseInt(count));
  }

  async function getBalanceCount() {
    const balance = await contract.balanceOf(localStorage.getItem('MetaMask Address'));
    setBalanceCount(parseInt(balance));

    document?.querySelector('#nft-loader')?.classList.add('display-none');
  }

  async function mintToken() {
    var isError = false;
    const contentID = 'Qmd8s77m3Yb9gzqUYPMRLmt9fD7DvBiYFbD9dN1mfng4GB';
    const metadataURI = `${contentID}/${totalNFT + 1}.json`;
    const result = await contract
      .payToMint(localStorage.getItem('MetaMask Address'), metadataURI, {
        value: ethers.utils.parseEther('0.15'),
      })
      .catch((error) => {
        if (error.code === 4001) {
          isError = true;
          return alert('User Declined Transaction.');
        }
        if (error.code === -32603) {
          isError = true;
          return alert('Please Reload Metamask Account.');
        }
        if (error.code === -32000) {
          isError = true;
          return alert('Please Add Fund to Your Account.');
        }
      });

    if (isError) {
      return;
    }

    document?.querySelector('#nft-loader')?.classList.remove('display-none');

    await result.wait().catch((error) => {
      console.log('XXXX');
      console.log(error);
    });
    getCountNFT();
    getBalanceCount();

    const mintData = {
      tokenSymbol: 'ETH -> NULP',
      tokenName: 'Ethereum',
      amount: '0.15',
      address: '0x9b88b0d2da010a5767a840085a5a7adb561daf65',
      hash: result.hash,
      date: Date.now(),
    };

    if (localStorage.getItem('Transaction History') !== null) {
      var transHistoryTemp = [];
      transHistoryTemp = JSON.parse(localStorage.getItem('Transaction History'));
      transHistoryTemp.push(mintData);
      localStorage.setItem('Transaction History', JSON.stringify(transHistoryTemp));
    } else {
      const tempAddress = [];
      tempAddress[0] = mintData;
      localStorage.setItem('Transaction History', JSON.stringify(tempAddress));
    }
  }

  document?.querySelector('.button-up')?.addEventListener('click', () => {
    document.body.scrollIntoView({
      behavior: 'smooth',
    });
  });

  return (
    <>
      {balanceCount ? (
        <>
          <div className="nft-blocks" id="nft-blocks-id">
            {Array(totalNFT)
              .fill(0)
              .map((_, i) => (
                <PolyCoin key={i} tokenID={i + 1} />
              ))}

            {totalNFT < 66 ? (
              <>
                <div className="nft-block" id="nft-loader">
                  <div className="nft-image-loader">
                    <div className="lds-grid">
                      <div></div>
                      <div></div>
                      <div></div>
                      <div></div>
                      <div></div>
                      <div></div>
                      <div></div>
                      <div></div>
                      <div></div>
                    </div>
                  </div>
                  <div className="nft-info">
                    <p>#?</p>
                    <button
                      onClick={() => {
                        alert('Please Wait. Your Transaction is Running.');
                      }}
                    >
                      View <FiExternalLink className="nft-url-icon" />
                    </button>
                  </div>
                </div>

                <div className="nft-block">
                  <div className="nft-image">
                    <img src={BlurredNewNFT} alt="" />
                  </div>
                  <div className="nft-info-new">
                    <button onClick={mintToken}>
                      Mint <SiEthereum className="eth-mint-icon" /> 0.15
                    </button>
                  </div>
                </div>
              </>
            ) : (
              <div className="nft-block-last">
                <p>Thank you!</p>
                <p>Last NFT was Already Minted</p>
              </div>
            )}
          </div>
        </>
      ) : (
        <div className="no-nft-block">
          <div className="nft-block">
            <div className="nft-image">
              <img src={BlurredNewNFT} alt="" />
            </div>
            <div className="nft-info-new">
              <button onClick={mintToken}>
                Mint <SiEthereum className="eth-mint-icon" /> 0.15
              </button>
            </div>
          </div>
          <p>Sorry, but we cant find any PolyCoin NFT in your wallet.</p>
        </div>
      )}
      {balanceCount < 12
        ? document?.querySelector('#nft-button-up')?.classList.add('display-none')
        : document?.querySelector('#nft-button-up')?.classList.remove('display-none')}
      <div className="button-up-block">
        <div className="button-up display-none" id="nft-button-up">
          <IoIosArrowUp className="button-up-icon" />
        </div>
      </div>
    </>
  );
}
