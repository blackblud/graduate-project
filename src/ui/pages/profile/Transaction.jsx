/* eslint-disable no-unused-vars */
import axios from 'axios';
import { ethers } from 'ethers';
import React, { useState, useEffect, useContext } from 'react';
import ethAddressFormatTrans from '../../../core/services/ethAddressFormatTrans';
import { ConnectContext } from '../../router/Index';
import { TokensContext } from '../Wallet';
import { BiSend } from 'react-icons/bi';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import ERC20ABI from '../../../core/config/ERC20ABI.json';
import tokensImages from '../../../core/store/tokensImages';
import Box from '@mui/material/Box';
import UnknownIcon from '../../../public/images/UnknownCoin.png';
import timestampTrans from '../../../core/services/timestampTrans';
import SpinerLoader from '../../../public/images/SpinerLoader.gif';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';
import { styled } from '@mui/material/styles';
import { FaAddressBook, FaRegAddressBook } from 'react-icons/fa';
import { MdGeneratingTokens, MdOutlineGeneratingTokens } from 'react-icons/md';

export default function Transaction() {
  const [accountAddress, setAccountAddress] = useState(null);
  const [accountBalance, setAccountBalance] = useState(null);

  const connectContext = useContext(ConnectContext);
  const tokensContext = useContext(TokensContext);

  const [tokenList, setTokenList] = useState(tokensContext.ERC20List);
  const [sendTokensList, setSendTokensList] = useState([
    {
      name: 'Loading...',
      symbol: 'LOAD',
      value: '',
    },
  ]);

  const [gasData, setGasData] = useState({
    result: {
      FastGasPrice: 5,
      ProposeGasPrice: 10,
      SafeGasPrice: 15,
    },
  });

  const [gasLevel, setGasLevel] = useState('standard');
  const [sendToken, setSentToken] = useState('');
  const [clearAutocomplete, setClearAutocomplete] = useState(true);

  const [transactionHistory, setTransactionHistory] = useState([]);

  useEffect(() => {
    if (localStorage.getItem('MetaMask Address') !== null) {
      setAccountAddress(localStorage.getItem('MetaMask Address'));
      setAccountBalance(localStorage.getItem('MetaMask Balance'));
    }
  }, [connectContext]);

  useEffect(() => {
    loadTokenInfo();
  }, []);

  async function loadTokenInfo() {
    let listERC20 = [
      {
        name: 'Ethereum',
        symbol: 'ETH',
        value: accountBalance,
        address: 'ETH',
      },
    ];

    for (let token of tokensContext.ERC20List) {
      const contract = new ethers.Contract(token, ERC20ABI, new ethers.providers.Web3Provider(window.ethereum));
      const balance = await contract.balanceOf(localStorage.getItem('MetaMask Address'));
      const symbol = await contract.symbol();
      const name = await contract.name();

      if (ethers.utils.formatEther(balance) > 0) {
        listERC20.push({
          name: name,
          symbol: symbol,
          value: ethers.utils.formatEther(balance),
          address: token,
        });
      }
    }
    setSendTokensList(listERC20);
  }

  useEffect(() => {
    axios
      .get('https://api.etherscan.io/api', {
        params: {
          module: 'gastracker',
          action: 'gasoracle',
          apikey: 'PMBK29YRB5QMSXHHE2WJFUDWBUUQTTPKPE',
        },
      })
      .then((res) => {
        setGasData(res.data);
      })
      .catch((error) => {
        console.log(error.response);
      });
  }, []);

  async function sendTransaction() {
    const provider = new ethers.providers.Web3Provider(window.ethereum);
    const signer = provider.getSigner();
    const tokenName = sendToken.name;
    const tokenSymbol = sendToken.symbol;
    const tokenAddress = sendToken.address;
    const tokenValue = sendToken.value;
    const tokenAmount = document.querySelector('#token-amount').value;
    const accountAddress = document.querySelector('#account-address').value;

    if (
      !sendToken ||
      sendToken === '' ||
      !tokenAmount ||
      tokenAmount === 0 ||
      tokenAmount === '' ||
      !accountAddress ||
      accountAddress === 0 ||
      accountAddress === '' ||
      !gasLevel
    ) {
      alert('One of The Input Fields is Blank or Incorrect.');
      return;
    }

    let transactionGas;
    if (gasLevel === 'slow') {
      transactionGas = gasData.result.SafeGasPrice;
    } else if (gasLevel === 'fast') {
      transactionGas = gasData.result.FastGasPrice;
    } else {
      transactionGas = gasData.result.ProposeGasPrice;
    }

    if (!ethers.utils.isAddress(accountAddress)) {
      alert('Wrong Account Address. Please check again.');
      return;
    }

    let transReceipt;
    if (tokenSymbol === 'ETH') {
      if (tokenAmount > accountBalance) {
        alert("You don't have enough Ethereum.");
        return;
      }
      transReceipt = await signer
        .sendTransaction({
          to: accountAddress,
          value: ethers.utils.parseEther(tokenAmount),
          gasPrice: parseInt(transactionGas + '000000000'),
        })
        .catch((error) => {
          alert('Error:\n' + error.message);
        });
    } else {
      if (tokenAmount > tokenValue) {
        alert("You don't have enough " + tokenName + '.');
        return;
      }
      const contract = new ethers.Contract(tokenAddress, ERC20ABI, signer);
      transReceipt = await contract
        .transfer(accountAddress, ethers.utils.parseEther(tokenAmount), { gasPrice: parseInt(transactionGas + '000000000') })
        .catch((error) => {
          alert('Error:\n' + error.message);
        });
    }

    const transData = {
      tokenSymbol: tokenSymbol,
      tokenName: tokenName,
      amount: tokenAmount,
      address: accountAddress,
      hash: transReceipt.hash,
      date: Date.now(),
    };

    if (localStorage.getItem('Transaction History') !== null) {
      var transHistoryTemp = [];
      transHistoryTemp = JSON.parse(localStorage.getItem('Transaction History'));
      transHistoryTemp.push(transData);

      localStorage.setItem('Transaction History', JSON.stringify(transHistoryTemp));
    } else {
      const tempAddress = [];
      tempAddress[0] = transData;
      localStorage.setItem('Transaction History', JSON.stringify(tempAddress));
    }

    setTransactionHistory((transactionHistory) => [...transactionHistory, transData]);

    setClearAutocomplete((clearAutocomplete) => !clearAutocomplete);
    document.querySelector('#token-amount').value = '';
    document.querySelector('#account-address').value = '';
  }

  const BootstrapTooltip = styled(({ className, ...props }) => <Tooltip {...props} arrow classes={{ popper: className }} />)(() => ({
    [`& .${tooltipClasses.arrow}`]: {
      color: '#363636',
    },
    [`& .${tooltipClasses.tooltip}`]: {
      backgroundColor: '#363636',
      fontSize: 14,
      maxWidth: 550,
      padding: 10,
    },
  }));

  useEffect(() => {
    if (localStorage.getItem('Transaction History') !== null) {
      var transHistory = [];
      transHistory = JSON.parse(localStorage.getItem('Transaction History'));
      setTransactionHistory(transHistory);
    }
  }, []);

  return (
    <div>
      <div className="transaction-send">
        <div className="transaction-send-blocks">
          <div className="transaction-send-block">
            <div className="transaction-label-block">
              <MdOutlineGeneratingTokens className="transaction-label-block-icon" />
              <p className="label-send-block">Token Address</p>
            </div>
            <Autocomplete
              disablePortal
              id="combo-box-demo"
              options={sendTokensList}
              onChange={(event, newValue) => {
                setSentToken(newValue);
              }}
              getOptionLabel={(option) => option.name}
              isOptionEqualToValue={(option, value) => option.id === value.id}
              sx={{ width: 280 }}
              key={clearAutocomplete}
              renderInput={(params) => <TextField {...params} label="Token Address" />}
              renderOption={(props, option) => (
                <Box component="li" sx={{ '& > img': { mr: 2, flexShrink: 0 } }} {...props}>
                  {option.symbol === 'LOAD' ? (
                    <>
                      <img loading="lazy" width="20" src={SpinerLoader} alt="" />
                      <p className="currencies-row">{option.name}</p>
                    </>
                  ) : tokensImages.get(option.symbol) ? (
                    <>
                      <img loading="lazy" width="20" src={tokensImages.get(option.symbol)} alt="" />
                      <p className="currencies-row">
                        {option.name}
                        <span>{option.symbol.toUpperCase()}</span>
                      </p>
                    </>
                  ) : (
                    <>
                      <img loading="lazy" width="20" src={UnknownIcon} alt="" />
                      <p className="currencies-row">
                        {option.name}
                        <span>{option.symbol.toUpperCase()}</span>
                      </p>
                    </>
                  )}
                </Box>
              )}
            />
          </div>
          <div className="transaction-send-block">
            <ToggleButtonGroup
              color="primary"
              value={gasLevel}
              exclusive
              onChange={(event, gas) => {
                setGasLevel(gas);
              }}
              sx={{ width: 280 }}
            >
              <ToggleButton value="slow">
                <p>Slow</p>
                <p>{gasData.result.SafeGasPrice} Gwei</p>
              </ToggleButton>
              <ToggleButton value="standard">
                <p>Standard</p>
                <p>{gasData.result.ProposeGasPrice} Gwei</p>
              </ToggleButton>
              <ToggleButton value="fast">
                <p>Fast</p>
                <p>{gasData.result.FastGasPrice} Gwei</p>
              </ToggleButton>
            </ToggleButtonGroup>
            <TextField
              id="token-amount"
              label="Amount"
              type="number"
              min="0"
              InputProps={{ inputProps: { min: 0, step: 0.01 } }}
              variant="outlined"
              sx={{ width: 280 }}
            />
          </div>
          <div className="transaction-send-block">
            <div className="transaction-label-block">
              <FaRegAddressBook />
              <p className="label-send-block">Account Address</p>
            </div>
            <TextField id="account-address" label="Account Address" variant="outlined" sx={{ width: 280 }} />
          </div>
        </div>
        <div className="transaction-send-button">
          <button onClick={sendTransaction}>
            Send Transaction <BiSend className="send-icon" />
          </button>
        </div>
      </div>
      <div className="transaction-history-block">
        <table>
          <colgroup>
            <col />
            <col style={{ width: 240 + 'px' }} />
            <col style={{ width: 200 + 'px' }} />
            <col style={{ width: 200 + 'px' }} />
            <col style={{ width: 200 + 'px' }} />
            <col style={{ width: 160 + 'px' }} />
          </colgroup>
          <thead>
            <tr>
              <th>#</th>
              <th>Token</th>
              <th>Amount</th>
              <th>To Address</th>
              <th>Hash</th>
              <th>Date</th>
            </tr>
          </thead>
          {transactionHistory === null ? (
            <tbody>
              <tr className="loader-anim-row">
                <td colSpan="11">
                  <div className="loader-anim-profile">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                  </div>
                </td>
              </tr>
            </tbody>
          ) : (
            <tbody id="table-transaction-insert">
              {transactionHistory.map((trans, i) => (
                <tr key={i}>
                  {}
                  <td className="currency-rank">{i + 1}</td>
                  <td>
                    <div className="transaction-name">
                      <img className="transaction-image" src={tokensImages.get(trans.tokenSymbol)} alt="" />
                      <p>{trans.tokenName}</p>
                      <span>{trans.tokenSymbol}</span>
                    </div>
                  </td>
                  <td>{trans.amount}</td>
                  <td>
                    <BootstrapTooltip placement="bottom" arrow title={trans.address}>
                      <a href={'https://rinkeby.etherscan.io/address/' + trans.address} target="_blank" rel="noreferrer">
                        {ethAddressFormatTrans(trans.address)}
                      </a>
                    </BootstrapTooltip>
                  </td>
                  <td>
                    <BootstrapTooltip placement="bottom" arrow title={trans.hash}>
                      <a href={'https://rinkeby.etherscan.io/tx/' + trans.hash} target="_blank" rel="noreferrer">
                        {ethAddressFormatTrans(trans.hash)}
                      </a>
                    </BootstrapTooltip>
                  </td>
                  <td>{timestampTrans(trans.date)}</td>
                </tr>
              ))}
            </tbody>
          )}
        </table>
      </div>
    </div>
  );
}
