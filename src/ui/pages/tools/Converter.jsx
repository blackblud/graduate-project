import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { FaArrowRight, FaSortDown, FaSortUp } from 'react-icons/fa';
import UnknownIcon from '../../../public/images/UnknownCoin.png';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import supportedCurrencies from '../../../core/store/supportedCurrencies';
import numberCommas from '../../../core/services/numberCommas';
import GoldBar from '../../../public/images/GoldBar.png';
import SilverBar from '../../../public/images/SilverBar.png';
import IMFIcon from '../../../public/images/IMFIcon.png';

export default function Converter() {
  const [exchangeValue, setExchangeValue] = useState(' ');
  const [supportedValue, setSupportedValue] = useState(null);
  const [exchangeList, setExchangeList] = useState([
    {
      id: '1',
      symbol: '2',
      name: '3',
    },
    {
      id: '1',
      symbol: '2',
      name: '3',
    },
  ]);

  useEffect(() => {
    axios
      .get('https://api.coingecko.com/api/v3/coins/markets', {
        params: {
          vs_currency: 'usd',
          per_page: 250,
          sparkline: false,
          page: 1,
        },
      })
      .then((res) => {
        const TempExchangeList = [];

        for (let coin = 0; coin < 250; coin++) {
          var temp = {};

          temp.id = res.data[coin].id;
          temp.symbol = res.data[coin].symbol;
          temp.image = res.data[coin].image;
          temp.name = res.data[coin].name;
          temp.group = 'Cryptocurrencies';

          TempExchangeList.push(temp);
        }

        axios
          .get('https://api.coingecko.com/api/v3/coins/markets', {
            params: {
              vs_currency: 'usd',
              per_page: 250,
              sparkline: false,
              page: 2,
            },
          })
          .then((res) => {
            for (let coin = 0; coin < 250; coin++) {
              var temp = {};

              temp.id = res.data[coin].id;
              temp.symbol = res.data[coin].symbol;
              temp.image = res.data[coin].image;
              temp.name = res.data[coin].name;
              temp.group = 'Cryptocurrencies';

              TempExchangeList.push(temp);
            }

            setExchangeList(TempExchangeList);
          })
          .catch((error) => {
            console.log(error.response);
          });

        setExchangeList(TempExchangeList);
      })
      .catch((error) => {
        console.log(error.response);
      });
  }, []);

  const convertFunction = () => {
    if (exchangeValue === null) return;
    if (supportedValue === null) return;
    if (document.querySelector('#converter-input').value === '') return;

    axios
      .get('https://api.coingecko.com/api/v3/simple/price', {
        params: {
          ids: exchangeValue.id,
          vs_currencies: supportedValue.ticker,
          include_market_cap: true,
          include_24hr_vol: true,
          include_24hr_change: true,
          include_last_updated_at: true,
        },
      })
      .then((res) => {
        document.querySelector('#converter-output').value =
          document.querySelector('#converter-input').value * res.data[exchangeValue.id][supportedValue.ticker];

        document.querySelector('#conv-market-cap').innerText =
          numberCommas(Math.round(res.data[exchangeValue.id][supportedValue.ticker + '_market_cap'])) + ' ' + supportedValue.sign;
        document.querySelector('#conv-volume').innerText =
          numberCommas(Math.round(res.data[exchangeValue.id][supportedValue.ticker + '_24h_vol'])) + ' ' + supportedValue.sign;
        document.querySelector('#conv-price-change').innerText =
          numberCommas(Math.abs(res.data[exchangeValue.id][supportedValue.ticker + '_24h_change'].toFixed(3))) + ' ' + supportedValue.sign;

        document.querySelector('#conv-market-cap').classList.remove('blured-text');
        document.querySelector('#conv-volume').classList.remove('blured-text');
        document.querySelector('#conv-price-change-p').classList.remove('blured-text');

        document.querySelector('#price-change-arrow-up').classList.add('display-none');
        document.querySelector('#price-change-arrow-down').classList.add('display-none');

        if (res.data[exchangeValue.id][supportedValue.ticker + '_24h_change'] > 0) {
          document.querySelector('#price-change-arrow-up').classList.remove('display-none');
        } else if (res.data[exchangeValue.id][supportedValue.ticker + '_24h_change'] < 0) {
          document.querySelector('#price-change-arrow-down').classList.remove('display-none');
        }
      })
      .catch((error) => {
        console.log(error.response);
      });
  };

  const clearConverter = () => {
    const value = document.querySelector('#converter-input').value;
    if (value === '') document.querySelector('#converter-output').value = '';
  };

  return (
    <div className="wrapper tools-page">
      <div className="converter-block">
        <div className="con-converter">
          <div className="converter-left">
            <p>Select Cryptocurrencies</p>
            <div className="select-currency">
              <img id="exchange-image" src={UnknownIcon} alt="" />
              <div className="dropdown-currency">
                <Autocomplete
                  id="country-select-demo"
                  options={exchangeList}
                  autoHighlight
                  groupBy={(option) => option.group}
                  onChange={(event, newValue) => {
                    if (newValue !== null) {
                      setExchangeValue(newValue);
                      document.querySelector('#exchange-image').src = newValue.image;
                    } else {
                      setExchangeValue(' ');
                      document.querySelector('#exchange-image').src = UnknownIcon;
                    }
                  }}
                  getOptionLabel={(option) => option.name}
                  renderOption={(props, option) => (
                    <Box component="li" sx={{ '& > img': { mr: 2, flexShrink: 0 } }} {...props}>
                      <img loading="lazy" width="20" src={option.image} alt="" />
                      <p className="currencies-row">
                        {option.name}
                        <span>{option.symbol.toUpperCase()}</span>
                      </p>
                    </Box>
                  )}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      label="Select Currency"
                      inputProps={{
                        ...params.inputProps,
                        autoComplete: 'new-password',
                      }}
                    />
                  )}
                />
              </div>
            </div>
            <div className="form__group field">
              <input
                type="number"
                className="form__field"
                placeholder="Input Amount"
                name="amount"
                required
                min="0"
                step="0.001"
                id="converter-input"
                onChange={clearConverter}
              />
              <label htmlFor="converter-input" className="form__label">
                Input Amount
              </label>
            </div>
          </div>
          <div className="converter-center" onClick={convertFunction}>
            <FaArrowRight />
          </div>
          <div className="converter-right">
            <p>Select Fiat Currency</p>
            <div className="select-currency">
              <img id="support-image" src={UnknownIcon} alt="" />
              <div className="dropdown-currency">
                <Autocomplete
                  id="country-select-demo"
                  options={supportedCurrencies}
                  autoHighlight
                  groupBy={(option) => option.group}
                  onChange={(event, newValue) => {
                    if (newValue !== null) {
                      setSupportedValue(newValue);

                      newValue.image !== ''
                        ? (document.querySelector('#support-image').src = newValue.image)
                        : newValue.code !== ''
                        ? (document.querySelector('#support-image').src = `https://flagcdn.com/w40/${newValue.code?.toLowerCase()}.png`)
                        : newValue.label === 'Gold'
                        ? (document.querySelector('#support-image').src = GoldBar)
                        : newValue.label === 'Silver'
                        ? (document.querySelector('#support-image').src = SilverBar)
                        : newValue.label === 'International Monetary Fund'
                        ? (document.querySelector('#support-image').src = IMFIcon)
                        : (document.querySelector('#support-image').src = UnknownIcon);
                    } else {
                      setSupportedValue(null);
                      document.querySelector('#support-image').src = UnknownIcon;
                    }
                  }}
                  getOptionLabel={(option) => option.label}
                  renderOption={(props, option) => (
                    <Box component="li" sx={{ '& > img': { mr: 2, flexShrink: 0 } }} {...props}>
                      {option.image !== '' ? (
                        <img loading="lazy" width="20" src={option.image} alt="" />
                      ) : option.code !== '' ? (
                        <img loading="lazy" width="20" src={`https://flagcdn.com/w20/${option.code?.toLowerCase()}.png`} alt="" />
                      ) : option.label === 'Gold' ? (
                        <img loading="lazy" width="20" src={GoldBar} alt="" />
                      ) : option.label === 'Silver' ? (
                        <img loading="lazy" width="20" src={SilverBar} alt="" />
                      ) : option.label === 'International Monetary Fund' ? (
                        <img loading="lazy" width="20" src={IMFIcon} alt="" />
                      ) : (
                        <img loading="lazy" width="20" src={UnknownIcon} alt="" />
                      )}

                      <p className="currencies-row">
                        {option.label}
                        <span>{option.ticker.toUpperCase()}</span>
                      </p>
                    </Box>
                  )}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      label="Select Currency"
                      inputProps={{
                        ...params.inputProps,
                        autoComplete: 'new-password',
                      }}
                    />
                  )}
                />
              </div>
            </div>
            <div className="form__group field">
              <input type="input" className="form__field" placeholder="Amount" name="amount" required disabled id="converter-output" />
              <label className="form__label">Output Amount</label>
            </div>
          </div>
        </div>

        <div className="con-info">
          <div className="con-info-block">
            <p>
              Market Cap <span className="block-24h-big">All</span>
            </p>
            <p id="conv-market-cap" className="blured-text blured-text-trans">
              1,000,000,000 $
            </p>
          </div>
          <div className="con-info-block">
            <p>
              Volume <span className="block-24h-big">24h</span>
            </p>
            <p id="conv-volume" className="blured-text blured-text-trans">
              1,000,000 $
            </p>
          </div>
          <div className="con-info-block">
            <p>
              Price Change <span className="block-24h-big">24h</span>
            </p>
            <p id="conv-price-change-p" className="blured-text blured-text-trans">
              <FaSortUp id="price-change-arrow-up" className="display-none" />
              <FaSortDown id="price-change-arrow-down" className="display-none" />
              <span id="conv-price-change">&uarr; 1,000 $</span>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
