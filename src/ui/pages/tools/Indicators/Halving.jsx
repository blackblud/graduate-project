import axios from 'axios';
import React, { useState, useEffect } from 'react';
import numberCommas from '../../../../core/services/numberCommas';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';
import { styled } from '@mui/material/styles';

export default function Halving() {
  const [halvingBlock, setHalvingBlock] = useState(725448);

  useEffect(() => {
    var end = new Date('03/28/2024 08:00 AM');

    var _second = 1000;
    var _minute = _second * 60;
    var _hour = _minute * 60;
    var _day = _hour * 24;
    var DateTimer;

    function showRemaining() {
      var now = new Date();
      var distance = end - now;
      if (distance < 0) {
        clearInterval(DateTimer);
        document.getElementById('countdown').innerHTML = 'EXPIRED!';

        return;
      }
      var days = Math.floor(distance / _day);
      var hours = Math.floor((distance % _day) / _hour);
      var minutes = Math.floor((distance % _hour) / _minute);
      var seconds = Math.floor((distance % _minute) / _second);

      document.getElementById('countdown-days').innerHTML = days;
      document.getElementById('countdown-hours').innerHTML = hours;
      document.getElementById('countdown-min').innerHTML = minutes;
      document.getElementById('countdown-sec').innerHTML = seconds;
    }

    DateTimer = setInterval(showRemaining, 1000);

    return () => {
      clearInterval(DateTimer);
    };
  }, []);

  useEffect(() => {
    axios
      .get('https://blockchain.info/latestblock')
      .then((res) => {
        setHalvingBlock(res.data.height);
      })
      .catch((error) => {
        console.log(error.response);
      });
  }, []);

  const BootstrapTooltip = styled(({ className, ...props }) => <Tooltip {...props} arrow classes={{ popper: className }} />)(() => ({
    [`& .${tooltipClasses.arrow}`]: {
      color: '#363636',
    },
    [`& .${tooltipClasses.tooltip}`]: {
      backgroundColor: '#363636',
      fontSize: 14,
      maxWidth: 456,
      padding: 10,
    },
  }));

  return (
    <div className="indicator-halving">
      <BootstrapTooltip
        placement="bottom"
        arrow
        title="Bitcoin halving is the process of halving the rewards of mining Bitcoin after each set of 210,000 blocks is mined. By reducing the rewards of mining Bitcoin as more blocks are mined, a Bitcoin halving limits the supply of new coins, so prices could rise if demand remains strong."
      >
        <p className="indicator-header">Bitcoin Halving</p>
      </BootstrapTooltip>

      <div className="halving-block">
        <p> Bitcoin block reward will decrease from 6.25 to 3.125 coins in approximately</p>
        <div className="halving-time">
          <div className="time-block">
            <p id="countdown-days">955</p>
            Days
          </div>
          <div className="time-block">
            <p id="countdown-hours">12</p>Hours
          </div>
          <div className="time-block">
            <p id="countdown-min">25</p>Minutes
          </div>
          <div className="time-block">
            <p id="countdown-sec">35</p>Seconds
          </div>
        </div>
        <div className="halving-slider">
          <div className="slider-slider">
            <input id="slider-block" type="range" min={630000} max={840000} value={840000 - (840000 - halvingBlock)} onChange={() => {}} />
          </div>
          <div className="slider-info">
            <div>
              Current Block
              <p>{numberCommas(840000 - (840000 - halvingBlock))}</p>
            </div>
            <div>
              Next Halving
              <p>{numberCommas(840000)}</p>
            </div>
            <div>
              Blocks Left
              <p>{numberCommas(840000 - halvingBlock)}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
