import axios from 'axios';
import React, { useEffect, useState } from 'react';
import GaugeChart from 'react-gauge-chart';
import FearChart from './FearChart';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';
import { styled } from '@mui/material/styles';

export default function FearGreed() {
  const [fearData, setFearData] = useState({
    data: [{ value: 0, value_classification: 'Neutral', timestamp: 0, time_until_update: 0 }],
  });

  const [fearChartData, setfearChartData] = useState({
    data: [{ value: 0, value_classification: 'Neutral', timestamp: 0, time_until_update: 0 }],
  });

  useEffect(() => {
    axios
      .get('https://api.alternative.me/fng/', {
        params: {
          limit: 31,
        },
      })
      .then((res) => {
        setFearData(res.data);
      })
      .catch((error) => {
        console.log(error.response);
      });
  }, []);

  useEffect(() => {
    axios
      .get('https://api.alternative.me/fng/', {
        params: {
          limit: 500,
        },
      })
      .then((res) => {
        setfearChartData(res.data);
      })
      .catch((error) => {
        console.log(error.response);
      });
  }, []);

  useEffect(() => {
    var timeleft = fearData.data[0].time_until_update;
    var downloadTimer = setInterval(function () {
      if (timeleft <= 0) {
        clearInterval(downloadTimer);
        document.location.reload();
      } else {
        var sec_num = parseInt(timeleft, 10);
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - hours * 3600) / 60);
        var seconds = sec_num - hours * 3600 - minutes * 60;

        if (minutes < 10) {
          minutes = '0' + minutes;
        }
        if (seconds < 10) {
          seconds = '0' + seconds;
        }

        document.getElementById('next-update').innerHTML = hours + ' Hours, ' + minutes + ' Minutes, ' + seconds + ' Seconds';
      }
      timeleft -= 1;
    }, 1000);

    return () => {
      clearInterval(downloadTimer);
    };
  }, [fearData]);

  const BootstrapTooltip = styled(({ className, ...props }) => <Tooltip {...props} arrow classes={{ popper: className }} />)(() => ({
    [`& .${tooltipClasses.arrow}`]: {
      color: '#363636',
    },
    [`& .${tooltipClasses.tooltip}`]: {
      backgroundColor: '#363636',
      fontSize: 14,
      maxWidth: 430,
      padding: 10,
    },
  }));

  return (
    <div className="indicator-fear">
      <BootstrapTooltip
        placement="bottom"
        arrow
        title="The fear and greed index is a tool based on the premise that excessive fear can result in stocks trading well below their intrinsic values
        while, at the same time, unbridled greed can result in stocks being bid up far above what they should be worth."
      >
        <p className="indicator-header">Fear and Greed Index</p>
      </BootstrapTooltip>
      <p className="indicator-desc"></p>
      <div className="fear-blocks">
        <div className="fear-block">
          <p>Fear &#38; Greed Index</p>
          <div className="fear-data">
            <GaugeChart
              id="gauge-chart3"
              formatTextValue={(value) => value + ''}
              nrOfLevels={5}
              textColor={'#363636'}
              needleColor={'#363636'}
              needleBaseColor={'#363636'}
              hideText={true}
              colors={['#ff4242', '#ff8000', '#ffd700', '#50e750', '#36bdff']}
              arcWidth={0.35}
              arcPadding={0.035}
              percent={fearData.data[0].value / 100}
            />

            {fearData.data[0].value_classification === 'Extreme Fear' ? (
              <div className="fear-data-value">
                <div className="gauge-value background-color-ext-fear">{fearData.data[0].value}</div>
                <p className="color-ext-fear">{fearData.data[0].value_classification}</p>
              </div>
            ) : fearData.data[0].value_classification === 'Fear' ? (
              <div className="fear-data-value">
                <div className="gauge-value background-color-fear">{fearData.data[0].value}</div>
                <p className="color-fear">{fearData.data[0].value_classification}</p>
              </div>
            ) : fearData.data[0].value_classification === 'Neutral' ? (
              <div className="fear-data-value">
                <div className="gauge-value background-color-neutral">{fearData.data[0].value}</div>
                <p className="color-neutral">{fearData.data[0].value_classification}</p>
              </div>
            ) : fearData.data[0].value_classification === 'Greed' ? (
              <div className="fear-data-value">
                <div className="gauge-value background-color-greed">{fearData.data[0].value}</div>
                <p className="color-greed">{fearData.data[0].value_classification}</p>
              </div>
            ) : fearData.data[0].value_classification === 'Extreme Greed' ? (
              <div className="fear-data-value">
                <div className="gauge-value background-color-ext-greed">{fearData.data[0].value}</div>
                <p className="color-ext-greed">{fearData.data[0].value_classification}</p>
              </div>
            ) : (
              ''
            )}
          </div>
        </div>
        <div className="fear-block">
          <p>Historical Values</p>
          <div className="fear-data-historical">
            {fearData.data[0].value_classification === 'Extreme Fear' ? (
              <div className="historical-value">
                <div>
                  <p>Now</p>
                  <p className="color-ext-fear">{fearData.data[0].value_classification}</p>
                </div>
                <div className="background-color-ext-fear">{fearData.data[0].value}</div>
              </div>
            ) : fearData.data[0].value_classification === 'Fear' ? (
              <div className="historical-value">
                <div>
                  <p>Now</p>
                  <p className="color-fear">{fearData.data[0].value_classification}</p>
                </div>
                <div className="background-color-fear">{fearData.data[0].value}</div>
              </div>
            ) : fearData.data[0].value_classification === 'Neutral' ? (
              <div className="historical-value">
                <div>
                  <p>Now</p>
                  <p className="color-neutral">{fearData.data[0].value_classification}</p>
                </div>
                <div className="background-color-neutral">{fearData.data[0].value}</div>
              </div>
            ) : fearData.data[0].value_classification === 'Greed' ? (
              <div className="historical-value">
                <div>
                  <p>Now</p>
                  <p className="color-greed">{fearData.data[0].value_classification}</p>
                </div>
                <div className="background-color-greed">{fearData.data[0].value}</div>
              </div>
            ) : fearData.data[0].value_classification === 'Extreme Greed' ? (
              <div className="historical-value">
                <div>
                  <p>Now</p>
                  <p className="color-ext-greed">{fearData.data[0].value_classification}</p>
                </div>
                <div className="background-color-ext-greed">{fearData.data[0].value}</div>
              </div>
            ) : (
              ''
            )}

            {fearData.data[1]?.value_classification === 'Extreme Fear' ? (
              <div className="historical-value">
                <div>
                  <p>Yesterday</p>
                  <p className="color-ext-fear">{fearData.data[1]?.value_classification}</p>
                </div>
                <div className="background-color-ext-fear">{fearData.data[1]?.value}</div>
              </div>
            ) : fearData.data[1]?.value_classification === 'Fear' ? (
              <div className="historical-value">
                <div>
                  <p>Yesterday</p>
                  <p className="color-fear">{fearData.data[1]?.value_classification}</p>
                </div>
                <div className="background-color-fear">{fearData.data[1]?.value}</div>
              </div>
            ) : fearData.data[1]?.value_classification === 'Neutral' ? (
              <div className="historical-value">
                <div>
                  <p>Yesterday</p>
                  <p className="color-neutral">{fearData.data[1]?.value_classification}</p>
                </div>
                <div className="background-color-neutral">{fearData.data[1]?.value}</div>
              </div>
            ) : fearData.data[1]?.value_classification === 'Greed' ? (
              <div className="historical-value">
                <div>
                  <p>Yesterday</p>
                  <p className="color-greed">{fearData.data[1]?.value_classification}</p>
                </div>
                <div className="background-color-greed">{fearData.data[1]?.value}</div>
              </div>
            ) : fearData.data[1]?.value_classification === 'Extreme Greed' ? (
              <div className="historical-value">
                <div>
                  <p>Yesterday</p>
                  <p className="color-ext-greed">{fearData.data[1]?.value_classification}</p>
                </div>
                <div className="background-color-ext-greed">{fearData.data[1]?.value}</div>
              </div>
            ) : (
              ''
            )}

            {fearData.data[7]?.value_classification === 'Extreme Fear' ? (
              <div className="historical-value">
                <div>
                  <p>Last week</p>
                  <p className="color-ext-fear">{fearData.data[7]?.value_classification}</p>
                </div>
                <div className="background-color-ext-fear">{fearData.data[7]?.value}</div>
              </div>
            ) : fearData.data[7]?.value_classification === 'Fear' ? (
              <div className="historical-value">
                <div>
                  <p>Last week</p>
                  <p className="color-fear">{fearData.data[7]?.value_classification}</p>
                </div>
                <div className="background-color-fear">{fearData.data[7]?.value}</div>
              </div>
            ) : fearData.data[7]?.value_classification === 'Neutral' ? (
              <div className="historical-value">
                <div>
                  <p>Last week</p>
                  <p className="color-neutral">{fearData.data[7]?.value_classification}</p>
                </div>
                <div className="background-color-neutral">{fearData.data[7]?.value}</div>
              </div>
            ) : fearData.data[7]?.value_classification === 'Greed' ? (
              <div className="historical-value">
                <div>
                  <p>Last week</p>
                  <p className="color-greed">{fearData.data[7]?.value_classification}</p>
                </div>
                <div className="background-color-greed">{fearData.data[7]?.value}</div>
              </div>
            ) : fearData.data[7]?.value_classification === 'Extreme Greed' ? (
              <div className="historical-value">
                <div>
                  <p>Last week</p>
                  <p className="color-ext-greed">{fearData.data[7].value_classification}</p>
                </div>
                <div className="background-color-ext-greed">{fearData.data[7].value}</div>
              </div>
            ) : (
              ''
            )}

            {fearData.data[30]?.value_classification === 'Extreme Fear' ? (
              <div className="historical-value">
                <div>
                  <p>Last month</p>
                  <p className="color-ext-fear">{fearData.data[30]?.value_classification}</p>
                </div>
                <div className="background-color-ext-fear">{fearData.data[30]?.value}</div>
              </div>
            ) : fearData.data[30]?.value_classification === 'Fear' ? (
              <div className="historical-value">
                <div>
                  <p>Last month</p>
                  <p className="color-fear">{fearData.data[30]?.value_classification}</p>
                </div>
                <div className="background-color-fear">{fearData.data[30]?.value}</div>
              </div>
            ) : fearData.data[30]?.value_classification === 'Neutral' ? (
              <div className="historical-value">
                <div>
                  <p>Last month</p>
                  <p className="color-neutral">{fearData.data[30]?.value_classification}</p>
                </div>
                <div className="background-color-neutral">{fearData.data[30]?.value}</div>
              </div>
            ) : fearData.data[30]?.value_classification === 'Greed' ? (
              <div className="historical-value">
                <div>
                  <p>Last month</p>
                  <p className="color-greed">{fearData.data[30]?.value_classification}</p>
                </div>
                <div className="background-color-greed">{fearData.data[30]?.value}</div>
              </div>
            ) : fearData.data[30]?.value_classification === 'Extreme Greed' ? (
              <div className="historical-value">
                <div>
                  <p>Last month</p>
                  <p className="color-ext-greed">{fearData.data[30]?.value_classification}</p>
                </div>
                <div className="background-color-ext-greed">{fearData.data[30]?.value}</div>
              </div>
            ) : (
              ''
            )}
          </div>
        </div>
        <div className="fear-block">
          <p>Next Update</p>
          <div className="fear-data-update">
            <p>The next update will happen in:</p>
            <p id="next-update">0 Hours, 0 Minutes, 0 Seconds</p>
          </div>
        </div>
      </div>
      <div className="fear-chart">
        <FearChart data={fearChartData.data} />
      </div>
    </div>
  );
}
