import React from 'react';
import { ResponsiveLine } from '@nivo/line';
import { BasicTooltip } from '@nivo/tooltip';

import timestampDateDay from '../../../../core/services/timestampDateDay';

export default function FearChart(props) {
  const LineTooltip = (props) => {
    if (props.point.data.y <= 20) {
      return <BasicTooltip id={props.point.data.xFormatted} value={props.point.data.y} color={'#ff4242'} enableChip />;
    } else if (props.point.data.y <= 40) {
      return <BasicTooltip id={props.point.data.xFormatted} value={props.point.data.y} color={'#ff8000'} enableChip />;
    } else if (props.point.data.y <= 60) {
      return <BasicTooltip id={props.point.data.xFormatted} value={props.point.data.y} color={'#ffd700'} enableChip />;
    } else if (props.point.data.y <= 80) {
      return <BasicTooltip id={props.point.data.xFormatted} value={props.point.data.y} color={'#50e750'} enableChip />;
    } else if (props.point.data.y <= 100) {
      return <BasicTooltip id={props.point.data.xFormatted} value={props.point.data.y} color={'#36bdff'} enableChip />;
    } else {
      return <BasicTooltip id={props.point.data.xFormatted} value={props.point.data.y} color={'#696969'} enableChip />;
    }
  };

  const FearData = [
    {
      id: 'CryptoFearChart',
      color: '#0ecb81',
      data: [],
    },
  ];

  if (props.data) {
    for (let id = 0; id < props.data.length; id++) {
      var temp = {};

      temp.x = timestampDateDay(parseInt(props.data[id].timestamp * 1000));
      temp.y = props.data[id].value;

      FearData[0].data.unshift(temp);
    }
  }

  return (
    <ResponsiveLine
      data={FearData}
      margin={{ top: 10, right: 50, bottom: 10, left: 50 }}
      xScale={{ type: 'point' }}
      yScale={{
        type: 'linear',
        min: 0,
        max: 100,
        stacked: true,
        reverse: false,
      }}
      curve={'linear'}
      theme={{
        tooltip: {
          container: {
            background: '#ffffff',
            color: '#333333',
            fontSize: 12,
            borderRadius: 6,
          },
        },
      }}
      colors={['#696969']}
      defs={[
        {
          id: 'gradientC',
          type: 'linearGradient',
          colors: [
            { offset: 0, color: '#36bdff' },
            { offset: 25, color: '#50e750' },
            { offset: 50, color: '#ffd700' },
            { offset: 75, color: '#ff8000' },
            { offset: 100, color: '#ff4242' },
          ],
        },
      ]}
      fill={[{ match: '*', id: 'gradientC' }]}
      axisTop={null}
      axisRight={null}
      axisBottom={null}
      axisLeft={{
        orient: 'left',
        tickSize: 5,
        tickPadding: 8,
        tickRotation: 0,
        legend: '',
        legendOffset: -40,
        legendPosition: 'middle',
      }}
      lineWidth={2}
      tooltip={LineTooltip}
      pointSize={0}
      enableGridX={false}
      pointLabelYOffset={-12}
      enableArea={true}
      areaOpacity={0.6}
      crosshairType="cross"
      useMesh={true}
      legends={[]}
      motionConfig="default"
    />
  );
}
