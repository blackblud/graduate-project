import React from 'react';
import FearGreed from './Indicators/FearGreed';
import Halving from './Indicators/Halving';

export default function Indicators() {
  return (
    <div className="wrapper">
      <div className="indicators-block">
        <FearGreed />
        <hr className="tools-hr" />
        <Halving />
      </div>
    </div>
  );
}
