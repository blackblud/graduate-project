import React from 'react';
import { Link } from 'react-router-dom';
import ConverterImage from '../../../public/images/ToolsConverter.png';
import IndicatorImage from '../../../public/images/ToolsIndicators.png';
import '../../style/tools.scss';

export default function Tools() {
  return (
    <div className="wrapper tools-page">
      <div className="tools-block">
        <Link to="/tools/converter">
          <div className="converter">
            <div className="converter-text">
              <p>Cryptocurrency Converter</p>
            </div>
            <img src={ConverterImage} alt="Converter" />
          </div>
        </Link>

        <Link to="/tools/indicators">
          <div className="indicator">
            <div className="indicator-text">
              <p>Cryptocurrency Indicators</p>
            </div>
            <img src={IndicatorImage} alt="Indicators" />
          </div>
        </Link>
      </div>
    </div>
  );
}
