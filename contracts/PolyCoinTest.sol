// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

contract PolyCoinTest is ERC721, ERC721URIStorage, Ownable {
    using Counters for Counters.Counter;

    Counters.Counter private _tokenIdCounter;

    mapping(string => address) existingsURIs;

    constructor() ERC721("PolyCoin", "NULP") {}

    function _baseURI() internal pure override returns (string memory) {
        return "https://gateway.pinata.cloud/ipfs/QmeDihKAYTBAMppMTEr51DWx9BG4L1x5aUUJGHeArhfT9E/";
    }

    function safeMint(address to, string memory uri) public onlyOwner {
        uint256 tokenId = _tokenIdCounter.current();
        _tokenIdCounter.increment();
        _safeMint(to, tokenId);
        _setTokenURI(tokenId, uri);
    }

    // The following functions are overrides required by Solidity.

    function _burn(uint256 tokenId) internal override(ERC721, ERC721URIStorage) {
        super._burn(tokenId);
    }

    function tokenURI(uint256 tokenId)
        public
        view
        override(ERC721, ERC721URIStorage)
        returns (string memory)
    {
        return super.tokenURI(tokenId);
    }

    function isContentOwned(address recipient1, string memory uri) public view returns (bool) {
        return existingsURIs[uri] == recipient1;
    }

    function payToMint(address recipient, string memory metadataURI) public payable returns (uint256){
        // require(existingsURIs[metadataURI] != 1, 'NFT already minted!');
        // Check if MetadataURI Already Inside Map

        require(existingsURIs[metadataURI] != address(0xEC9D26110B1A9EdaC6800A959326364dD27cC035), "NFT Already Minted");
        require(existingsURIs[metadataURI] != address(0xf39Fd6e51aad88F6F4ce6aB8827279cffFb92266), "NFT Already Minted");
        require(existingsURIs[metadataURI] != address(0x302C38Ef382ba2584ba526DD1c89bE49959bFFF0), "NFT Already Minted");
        require(existingsURIs[metadataURI] != address(0x4C91fcEb3f003D8412B8da42c0a1346d56d4BE18), "NFT Already Minted");
        require(existingsURIs[metadataURI] != address(0x70997970C51812dc3A010C7d01b50e0d17dc79C8), "NFT Already Minted");

        require(msg.value >= 0.15 ether, 'Need to pay up!');
        uint256 newItemId = _tokenIdCounter.current();
        _tokenIdCounter.increment();
        existingsURIs[metadataURI] = recipient;

        _mint(recipient, newItemId);
        _setTokenURI(newItemId, metadataURI);

        return newItemId;
    }

    function count() public view returns (uint256){
        return _tokenIdCounter.current();
    }

    function checkAddress(string memory uri) public view returns (bool) {

        if(existingsURIs[uri] == address(0x70997970C51812dc3A010C7d01b50e0d17dc79C8)){
            return true;
        } else {
            return false;
        }
    }
}