const { expect } = require('chai');
const { ethers } = require('hardhat');

describe('PolyCoin', function () {
  it('Mint and Transfer NFT', async function () {
    const PolyCoin = await ethers.getContractFactory('PolyCoin');
    const polycoin = await PolyCoin.deploy();
    await polycoin.deployed();

    ///////////////////////////////////////////////////////////////////////////

    var recipient = '0xf39Fd6e51aad88F6F4ce6aB8827279cffFb92266';
    var metadataURI = 'nft/1.png';

    const newlyMintedToken = await polycoin.payToMint(recipient, metadataURI, {
      value: ethers.utils.parseEther('0.15'),
    });

    await newlyMintedToken.wait();

    let balance1 = await polycoin.balanceOf(recipient);
    expect(balance1).to.equal(1);

    expect(await polycoin.isContentOwned(recipient, metadataURI)).to.equal(true);

    ///////////////////////////////////////////////////////////////////////////

    var recipient1 = '0x70997970c51812dc3a010c7d01b50e0d17dc79c8';
    var metadataURI1 = 'nft/1.png';

    const newlyMintedToken1 = await polycoin.payToMint(recipient1, metadataURI1, {
      value: ethers.utils.parseEther('0.15'),
    });

    await newlyMintedToken1.wait();

    expect(await polycoin.isContentOwned(recipient1, metadataURI1)).to.equal(true);

    // expect(await polycoin.checkAddress(metadataURI)).to.equal(true);

    // recipient = '0x70997970c51812dc3a010c7d01b50e0d17dc79c8';

    // expect(await polycoin.isContentOwned(recipient, metadataURI)).to.equal(false);

    ///////////////////////////////////////////////////////////////////////////

    // recipient = '0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266';
    // metadataURI = 'cid/test1.png';

    // const newlyMintedToken1 = await polycoin.payToMint(recipient, metadataURI, {
    //   value: ethers.utils.parseEther('0.15'),
    // });

    // await newlyMintedToken1.wait();

    // let balance2 = await polycoin.balanceOf(recipient);
    // expect(balance2).to.equal(2);

    ///////////////////////////////////////////////////////////////////////////
  });
});
