require('dotenv').config();

require('@nomiclabs/hardhat-etherscan');
require('@nomiclabs/hardhat-waffle');
require('hardhat-gas-reporter');
require('solidity-coverage');

// This is a sample Hardhat task. To learn how to create your own go to
// https://hardhat.org/guides/create-task.html
task('accounts', 'Prints the list of accounts', async (taskArgs, hre) => {
  const accounts = await hre.ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});

// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  solidity: '0.8.4',
  paths: {
    artifacts: './src/artifacts',
  },
  networks: {
    matic: {
      url: 'https://polygon-mumbai.g.alchemy.com/v2/XSOzOE6ycqKSPb3snzzMOZ7nBedLWUHL',
      accounts: ['3be3e39db958f78d88ea0353ede3882646a95bf8e8f4dbd6d8f524d68c35cf44'],
    },
    rinkeby: {
      url: 'https://eth-rinkeby.alchemyapi.io/v2/Iw87_RXwWoE7EAayT-9qicpqF-IeWkGw',
      accounts: ['ae94d73886955e4b252c798745760bf8f5b83eb87ef6d02a6c7d865814902fff'],
      gas: 2100000,
      gasPrice: 8000000000,
      saveDeployments: true,
    },
  },
};
