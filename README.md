# CoinFolio

Domain Main - [https://www.blackblud.me/](https://www.blackblud.me/)  
Domain IPFS - [https://rough-hat-7234.on.fleek.co](https://rough-hat-7234.on.fleek.co/)  
   
Project IPFS - [https://ipfs.io/ipfs/QmUMKSs65iii77adFmp8SPPYyGP4zK4R8tSM7esMCsqrxV](https://ipfs.io/ipfs/QmUMKSs65iii77adFmp8SPPYyGP4zK4R8tSM7esMCsqrxV)   
NFT Collection - [https://ipfs.io/ipfs/Qmd8s77m3Yb9gzqUYPMRLmt9fD7DvBiYFbD9dN1mfng4GB](https://ipfs.io/ipfs/Qmd8s77m3Yb9gzqUYPMRLmt9fD7DvBiYFbD9dN1mfng4GB)   

Web application for viewing statistics and working with cryptocurrencies based on interaction with Web 3.0

## Modules
### Cryptocurrency Statistics
*Statistics of world cryptocurrencies, divided into various categories.*  
*Information on the global crypto market, trends, company statistics and fiat value.*  
*Detailed information on each crypto token.*  
* Cryptocurrencies
* Cryptocurrency Categories
* CryptoCoin Search
* CryptoCoin Single Page
* Statistics:
  * Global Statistics
  * Category Statistics
  * Trending / Companies / Fiat BTC
### Whale Alert
*Real-time statistics of large crypto transfers.*  
*Map of transactions between exchanges and unknown wallets.*  
*Table of largest transfers in the last hour (over $500,000)*  
* Blockchain Status
* Transactions List (Real-Time)
* Transactions Map (Real-Time)
* Transactions Table (Last Hours > $500,000)
### Tools
*Useful tools for working with cryptocurrencies.*  
*Converter for conversion between crypto, fiat currencies, gold or silver.*  
*Indicators of Fear & Greed and countdown to bitcoin halving*  
* Converter
  * Crypto -> Crypto
  * Crypto -> Fiat
  * Crypto -> Gold / Silver / International Monetary Fund
  * Statistics Blocks (Market Cap / Volume/ Price Change)
* Indicators
  * Fear & Greed Index
  * Bitcoin Halving
### My Wallet
*Web 3.0. Connecting to your Metamask wallet.*  
*Display ETH balance, ERC-20 and any imported tokens*  
*Sending transactions with any ERC-20 Tokens*  
*Ability to mint random NFT from the PolyCoin collection*  
* MetaMask Connect
* MetaMask Balance
  * Ethereum
  * ERC-20 (Top 100)
  * ERC-20 (by Address)
* MetaMask Transactions
  * Send Transactions (ETH)
  * Send Transactions (ERC-20)
  * Transaction History
* MetaMask NFT
  * NFT Metadata & Polycoin Design
  * NFT Smart-Contract
  * NFT Viewing
  * NFT Minting
### Other
*IPFS - InterPlanetary File System*  
*IPFS - P2P network for storing and sharing data in a distributed file system.*  
*IPFS - stores and hosts the site, as well as all metadata of the NFT collection*  
* Page 404
* IPFS Deploy

## Getting Started (Hardhat Localhost Network)  
Steps on how to run a project on a local hardhat network.  
Don't forget to change your smart-contract address.

1. Install all project dependencies 
    ```console
    yarn
    ```
2. Start local server
    ```console
    yarn start
    ```
3. Run Hardhat Network
    ```console
    npx hardhat node
    ```
4. Deploy selected smart-contract on localhost
    ```console
    node run scripts/smart-contract.js --network localhost
    ```
5. Replace the received smart contract address in files: 
    * [NFT.jsx](/src/ui/pages/profile/NFT.jsx)
    * [PolyCoin.jsx](/src/ui/pages/profile/PolyCoin.jsx)

## NFT Collection PolyCoin NULP
### Generation #1
| Light | Dark |
| :-: | :-: |
| ![PolyCoin](/public/NFT/25.png)  | ![PolyCoin](/public/NFT/24.png) |
| ![PolyCoin](/public/NFT/44.png)  | ![PolyCoin](/public/NFT/32.png) | 
| ![PolyCoin](/public/NFT/35.png)  | ![PolyCoin](/public/NFT/55.png) |
| ![PolyCoin](/public/NFT/48.png)  | ![PolyCoin](/public/NFT/30.png) |
| ![PolyCoin](/public/NFT/38.png)  | ![PolyCoin](/public/NFT/16.png) |
| ![PolyCoin](/public/NFT/19.png)  | ![PolyCoin](/public/NFT/10.png) |
| ![PolyCoin](/public/NFT/29.png)  | ![PolyCoin](/public/NFT/57.png) |

### Generation #2
| Light Gradient | Dark Gradient |
| :-: | :-: |
| ![PolyCoin](/public/NFT/39.png)  | ![PolyCoin](/public/NFT/1.png) |
| ![PolyCoin](/public/NFT/64.png)  | ![PolyCoin](/public/NFT/8.png) |
| ![PolyCoin](/public/NFT/42.png)  | ![PolyCoin](/public/NFT/6.png) |
| ![PolyCoin](/public/NFT/20.png)  | ![PolyCoin](/public/NFT/41.png) |
| ![PolyCoin](/public/NFT/3.png)  | ![PolyCoin](/public/NFT/37.png) |
| ![PolyCoin](/public/NFT/14.png)  | ![PolyCoin](/public/NFT/27.png) |
| ![PolyCoin](/public/NFT/61.png)  | ![PolyCoin](/public/NFT/56.png) |

### Generation #3
| Ukrainian Flag | UPA Flag |
| :-: | :-: |
| ![PolyCoin](/public/NFT/53.png)  | ![PolyCoin](/public/NFT/15.png) |
| ![PolyCoin](/public/NFT/7.png)  | ![PolyCoin](/public/NFT/34.png) |

### Generation #4
| Lapis Lazuli                     | Rubellite                        | Tanzanite                        |
| :------------------------------: | :------------------------------: | :------------------------------: |
| ![PolyCoin](/public/NFT/4.gif)   | ![PolyCoin](/public/NFT/26.gif)  | ![PolyCoin](/public/NFT/49.gif)  |
| ![PolyCoin](/public/NFT/40.gif)  | ![PolyCoin](/public/NFT/65.gif)  | ![PolyCoin](/public/NFT/9.gif)   |
 


